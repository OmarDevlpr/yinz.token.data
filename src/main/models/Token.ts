import { Column, Entity } from "typeorm";
import { Model } from "@yinz/commons.data";

@Entity()
export default class Token extends Model {
    
    /* MODEL FIELDS */

    @Column()
    code: string;

    @Column()
    owner: string;    

    @Column({nullable: true})
    data: string;    

    @Column({ type: 'timestamptz' })
    startValidity: Date;

    @Column()
    validityDuration: number;    

    @Column()
    maxNumberOfUse: number;    

    @Column()
    numberOfUse: number;    
}
