import { Column, Entity } from "typeorm";
import { Model } from "@yinz/commons.data";

@Entity()
export default class ArchToken extends Model {

    /* MODEL FIELDS */

    @Column()
    code: string;

    @Column()
    owner: string;

    @Column({ nullable: true })
    data: string;

    @Column({ type: 'timestamptz' })
    startValidity: Date;

    @Column()
    validityDuration: number;

    @Column()
    maxNumberOfUse: number;

    @Column()
    numberOfUse: number;    

    @Column({ type: 'timestamptz' })
    archOn: Date;

    @Column({ enum: ["E", "B"] })
    status: "E" | "B";
}
