
import Container from "@yinz/container/ts/Container";
import Logger from "@yinz/commons/ts/Logger";
import AccessManager from "@yinz/access.data/ts/AccessManager";
import { TypeormDao } from "@yinz/commons.data";
import Token from "../models/Token";
import ArchToken from "../models/ArchToken";



const registerBeans = (container: Container) => {

    let logger = container.getBean<Logger>('logger');

    let accessManager = container.getBean<AccessManager>('accessManager');

    let tokenDao     = new TypeormDao<Token    > ({ model: Token    , accessManager, logger });
    let archTokenDao = new TypeormDao<ArchToken> ({ model: ArchToken, accessManager, logger });
    

    container.setBean('tokenDao'    , tokenDao);
    container.setBean('archTokenDao', archTokenDao);
    

};

const registerClazzes = (container: Container) => {

    container.setClazz('Token', Token);
    container.setClazz('ArchToken', ArchToken);    

};


export {
    registerBeans,
    registerClazzes
};