// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import ArchToken from "../../main/models/ArchToken";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const archTokenDao = container.getBean<TypeormDao<ArchToken>>('archTokenDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.TokenTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let archToken = new ArchToken();
        archToken.code = chance.string({ length: 10 });
        archToken.owner = chance.string({ length: 10 });
        archToken.startValidity = chance.date();
        archToken.validityDuration = chance.integer({ min: 90000, max: 100000000 });
        archToken.maxNumberOfUse = chance.integer({ min: 90000, max: 100000000 });
        archToken.archOn  = chance.date();
        archToken.status  = "E";
        archToken.numberOfUse  = 0;

        let archToken2 = new ArchToken();
        archToken2.code = chance.string({ length: 10 });
        archToken2.owner = chance.string({ length: 10 });
        archToken2.startValidity = chance.date();
        archToken2.validityDuration = chance.integer({ min: 90000, max: 100000000 });
        archToken2.maxNumberOfUse = chance.integer({ min: 90000, max: 100000000 });
        archToken2.archOn = chance.date();
        archToken2.status = "B";
        archToken2.numberOfUse = 0;



        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await archTokenDao.create(trans, archToken, options);
        let createAllResult = await archTokenDao.createAll(trans, [archToken2], options);
        let updateByFilterResult = await archTokenDao.updateByFilter(trans, { owner: createResult.owner }, { owner: createResult.owner }, options);
        let updateByIdResult = await archTokenDao.updateById(trans, { owner: createResult.owner }, createResult.id, options);
        let findAllResult = await archTokenDao.findAll(trans, options);
        let findByIdResult = await archTokenDao.findById(trans, createResult.id, options);
        let findByFilterResult = await archTokenDao.findByFilter(trans, { owner: archToken2.owner }, options);
        let deleteByFilterResult = await archTokenDao.deleteByFilter(trans, { owner: "dont" }, options);
        let deleteByIdREsult = await archTokenDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });



});
