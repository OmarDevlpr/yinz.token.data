import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');


describe('| access.data.di', function () {
    it('| should assemble the module', function () {
        try {
            // classes
            assert.ok(container.getClazz('Token'));
            assert.ok(container.getClazz('ArchToken'));            
            // beans
            assert.ok(container.getBean('tokenDao'));
            assert.ok(container.getBean('archTokenDao'));
            

        } catch (e) {
            if (e instanceof Exception) {
                assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
            }
            else {
                assert.ok(false, 'exception: ' + e.message + '\n' + e.stack);
            }
        }
    });
});
