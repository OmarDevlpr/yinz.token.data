// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Token from "../../main/models/Token";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const tokenDao = container.getBean<TypeormDao<Token>>('tokenDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.TokenTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let token = new Token();
        token.code  = chance.string({ length: 10 });
        token.owner = chance.string({ length: 10 });
        token.startValidity = chance.date();
        token.validityDuration = chance.integer({min: 90000, max: 100000000});
        token.maxNumberOfUse   = chance.integer({min: 90000, max: 100000000});
        token.numberOfUse      = 0;

        let token2 = new Token();
        token2.code = chance.string({ length: 10 });
        token2.owner = chance.string({ length: 10 });
        token2.startValidity = chance.date();
        token2.validityDuration = chance.integer({ min: 90000, max: 100000000 });
        token2.maxNumberOfUse = chance.integer({ min: 90000, max: 100000000 });
        token2.numberOfUse = 0;
        


        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await tokenDao.create(trans, token, options);
        let createAllResult = await tokenDao.createAll(trans, [token2], options);
        let updateByFilterResult = await tokenDao.updateByFilter(trans, { owner: createResult.owner }, { owner: createResult.owner }, options);
        let updateByIdResult = await tokenDao.updateById(trans, { owner: createResult.owner }, createResult.id, options);
        let findAllResult = await tokenDao.findAll(trans, options);
        let findByIdResult = await tokenDao.findById(trans, createResult.id, options);
        let findByFilterResult = await tokenDao.findByFilter(trans, { owner: token2.owner }, options);
        let deleteByFilterResult = await tokenDao.deleteByFilter(trans, { owner: "dont" }, options);
        let deleteByIdREsult = await tokenDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });



});
